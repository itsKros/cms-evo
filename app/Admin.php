<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticable;

class Admin extends Authenticable
{
    use Notifiable;
    
    // The authentication guard for admin
    protected $guard = 'admin';
     
    protected $fillable = ['email', 'password', ];
    
    protected $hidden = ['password', 'remember_token',];
}
